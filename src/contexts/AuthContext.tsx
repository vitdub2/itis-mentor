import React, {useState} from "react";
import {Component} from "../types/common";
import {API} from "../api";
import {LoginData, RegisterData} from "../types/auth";
import {clearUserId, getUserId, setUserId} from "../helpers/authHelper";
import {createError} from "../helpers/errorHelper";

type IAuthContext = {
    isAuth: boolean;
    auth: (data: LoginData) => Promise<any>;
    register: (data: RegisterData) => Promise<any>;
    loadUserData: () => Promise<any>;
    logout: () => void;
    userInfo: RegisterData;
}

export const AuthContext = React.createContext<IAuthContext>({
    isAuth: false,
    auth: () => new Promise(() => {}),
    register: () => new Promise(() => {}),
    logout: () => {},
    loadUserData: () => new Promise(() => {}),
    userInfo: {
        login: "",
        password: "",
        email: "",
        photoUrl: "",
    },
});
AuthContext.displayName = "AuthContext";

export const AuthContextProvider: Component = ({ children }) => {
    const userId: string = getUserId() || "";

    const [isAuth, setAuth] = useState(!!userId);
    const [userInfo, setUserInfo] = useState<RegisterData>({
        login: "",
        password: "",
        email: "",
        photoUrl: "",
    });

    const auth = (data: LoginData) => {
        return API.auth.login(data).then((resp) => {
            const {data: info} = resp;
            const user = info[0];
            if (user) {
                setUserId(user.id);
                setUserInfo(user);
                setAuth(true);
            } else {
                throw createError({_error: "Invalid login or password"});
            }
        });
    }

    const register = (data: RegisterData) => {
        return API.auth.register(data).then((resp) => {
            const {data: info} = resp;
            setUserId(info.id);
            setUserInfo(info);
            setAuth(true);
        });
    }

    const logout = () => {
        clearUserId();
        setAuth(false);
    }

    const loadUserData = () => {
        return API.auth.loadUserData(userId).then((resp) => {
            const {data: info} = resp;
            const user = info[0];
            if (user) {
                setUserId(user.id);
                setUserInfo(user);
                setAuth(true);
            } else {
                throw createError("Invalid user id");
            }
        }).catch(() => {
            logout();
        });
    }

    const value: IAuthContext = {
        isAuth,
        auth,
        logout,
        userInfo,
        register,
        loadUserData,
    };

    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    );
}
