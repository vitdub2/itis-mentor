import {Layout, Menu, MenuProps} from "antd";
import React, {FC, useContext, useMemo} from "react";
import {AuthContext} from "../contexts/AuthContext";
import {NavLink, useLocation} from "react-router-dom";
import { LoginOutlined, LogoutOutlined, UserOutlined, UserAddOutlined } from "@ant-design/icons";

const authItems: MenuProps[`items`] = [
    {
        key: "/login",
        label: <NavLink to="/login">
            Login
        </NavLink>,
        icon: <LoginOutlined />
    },
    {
        key: "/register",
        label: <NavLink to="/register">
            Register
        </NavLink>,
        icon: <UserAddOutlined />
    }
];

const commonItems: MenuProps[`items`] = [
    {
        key: "/logout",
        label: <NavLink to="/logout">
            Logout
        </NavLink>,
        icon: <LogoutOutlined />,
    }
];

export const Header: FC = () => {
    const {userInfo, isAuth} = useContext(AuthContext);

    const location = useLocation();

    const items = useMemo(() => {
        if (isAuth) {
            return [
                {
                    key: "/",
                    label: <NavLink to="/">
                        {userInfo.login}
                    </NavLink>,
                    icon: <UserOutlined />
                },
                ...commonItems,
            ]
        }
        return authItems;
    }, [isAuth, userInfo]);

    return (
        <Layout.Header>
            <Menu selectedKeys={[location.pathname]} items={items} theme="dark" mode="horizontal"/>
        </Layout.Header>
    );
}
