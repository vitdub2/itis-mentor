import { Outlet } from "react-router-dom";
import React, {FC} from "react";
import {Layout, Row, Col} from "antd";

export const Content: FC = () => {
    return (
        <Layout.Content>
            <Row>
              <Col span={22} offset={1}>
                <Outlet/>
              </Col>
            </Row>
        </Layout.Content>
    );
}
