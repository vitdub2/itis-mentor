import React from 'react';
import {Layout} from "antd";

export const Footer = () => {
    return (
        <Layout.Footer>
            ITIS Mentor. All rights reserved.
        </Layout.Footer>
    );
}
