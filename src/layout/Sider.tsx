import {BarsOutlined, ScheduleOutlined} from "@ant-design/icons";
import {Layout, Menu, MenuProps} from "antd";
import {FC, useState} from "react";
import {NavLink, useLocation} from "react-router-dom";

const menuItems: MenuProps["items"] = [
    {
        key: "/groups",
        label: <NavLink to="/groups">
            My groups
        </NavLink>,
        icon: <ScheduleOutlined />,
    },
    {
        key: "/groups/all",
        label: <NavLink to="/groups/all">
            All groups
        </NavLink>,
        icon: <BarsOutlined />,
    }
];

export const Sider: FC = () => {
    const [collapsed, setCollapsed] = useState(true);
    const handleCollapse = () => {
        setCollapsed(prev => !prev);
    }
    const location = useLocation();
    return (
        <Layout.Sider theme="light" collapsible collapsed={collapsed} onCollapse={handleCollapse}>
            <Menu
                theme="light"
                mode="vertical"
                defaultSelectedKeys={[location.pathname]}
                items={menuItems}
            />
        </Layout.Sider>
    );
};
