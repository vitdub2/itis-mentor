import {Layout} from "antd";
import {Component} from "../types/common";
import {CSSProperties} from "react";

type PageProps = {
    styles?: CSSProperties,
}

export const Page: Component<PageProps> = ({ children, styles }) => {
    return (
        <Layout style={{ minHeight: '100vh', ...styles }}>
            {children}
        </Layout>
    );
}
