import {FC, useContext, useEffect} from "react";
import { Layout } from 'antd';
import { Sider } from "./Sider";
import { Header } from "./Header";
import {Content} from "./Content";
import {Footer} from "./Footer";
import {Page} from "./Page";
import {AuthContext} from "../contexts/AuthContext";

export const Main: FC = () => {
    const {isAuth, loadUserData} = useContext(AuthContext);

    useEffect(() => {
        if (isAuth) {
            loadUserData();
        }
    }, [isAuth]);
    return (
        <Page>
            <Sider/>
            <Layout>
                <Header/>
                <Content/>
                <Footer/>
            </Layout>
        </Page>
    );
}
