import axios, {AxiosPromise, AxiosRequestConfig, Method} from "axios";

export const fetchApi = (url: string, params?: Record<string, any>, method: Method = "GET",): AxiosPromise => {
    const config: AxiosRequestConfig =  {
        url: url,
        baseURL: "http://localhost:8000",
        method: method,
    }

    if (params) {
        if (method === "GET") {
            config.params = params;
        } else {
            config.data = params;
        }
    }

    return axios(config);
}
