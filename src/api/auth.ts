import {fetchApi} from "./fetch";
import {LoginData, RegisterData} from "../types/auth";

export const authGroup = {
    loadUserData: (id: string) => fetchApi("auth", {id}, "GET"),
    login: (data: LoginData) => fetchApi("auth", data, "GET"),
    register: (data: RegisterData) => fetchApi("auth", data, "POST"),
    getUser: (id: string) => fetchApi("auth", {id}, "GET"),
};
