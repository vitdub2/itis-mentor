import {fetchApi} from "./fetch";
import {IGroupDetails} from "../pages/groups/GroupPage";

export const lessonGroup = {
    getLessons: () => fetchApi("groups"),
    getAllLessonInfo: () => fetchApi("groupsInfo"),
    getLessonInfo: (groupId :string) => fetchApi("groupsInfo", {groupId}, "GET"),
    getLesson: (id: string) => fetchApi("groups", {id}, "GET"),
    getTutoringLessons: (tutorId: string) => fetchApi("groups", {tutorId}, "GET"),
    getParticipatingLessons: (id: string) => {
        return fetchApi("groupsInfo").then(({data}) => {
            const preparedData = data.map((group: IGroupDetails) => ({
                ...group,
                peopleInfo: group.peopleInfo.map((obj) => obj.id),
            }));
            const participatingGroups = preparedData.filter((group: any) => group.peopleInfo.includes(+id));
            return Promise.all(participatingGroups.map((group: any) => fetchApi("groups", {id: group.id}, "GET").then(resp => resp.data[0])))
        })
    }
};
