import {authGroup} from "./auth";
import {lessonGroup} from "./lesson";

export const API = {
    auth: authGroup,
    group: lessonGroup,
}
