import {FC, useEffect, useState} from "react";

import {Avatar, Row, Col, Typography, Space} from "antd";
import {RegisterData} from "../types/auth";
import {NavLink, useParams} from "react-router-dom";
import {API} from "../api";
import {IGroup} from "./groups/MyGroupsPage";
import {GroupItem} from "../components/groups/GroupItem";

const { Title } = Typography;

export const UserPage: FC = () => {
    const [userInfo, setUserInfo] = useState<RegisterData>();
    const [tutoringGroups, setTutoringInfo] = useState<IGroup[]>();
    const [participatingGroups, setGroups] = useState<IGroup[]>();

    const {id} = useParams();

    useEffect(() => {
        if (id) {
            API.auth.getUser(id).then((resp) => {
                setUserInfo(resp.data[0]);
            });
            API.group.getTutoringLessons(id).then((resp) => {
                setTutoringInfo(resp.data);
            });
            API.group.getParticipatingLessons(id).then((data) => {
                setGroups(data);
            });
        }
    }, [id]);

    if (userInfo) {
        return (
            <>
                <Title>
                    {userInfo.login}
                </Title>
                <Row>
                    <Col>
                        <Avatar src={userInfo.photoUrl} size="large"/>
                    </Col>
                    <Col>
                        Email: {userInfo.email}
                    </Col>
                </Row>
                {tutoringGroups && tutoringGroups.length > 0 && (
                    <Col>
                        <Row>
                            <Title level={2}>
                                Tutoring in
                            </Title>
                        </Row>
                        <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                            {tutoringGroups.map(group => (
                                <NavLink to={`/groups/${group.id}`}>
                                    <GroupItem key={group.id} {...group} />
                                </NavLink>
                            ))}
                        </Space>
                    </Col>
                )}
                {participatingGroups && participatingGroups.length > 0 && (
                    <Col>
                        <Row>
                            <Title level={2}>
                                Taking part in
                            </Title>
                        </Row>
                        <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                            {participatingGroups.map(group => (
                                <NavLink to={`/groups/${group.id}`}>
                                    <GroupItem key={group.id} {...group} />
                                </NavLink>
                            ))}
                        </Space>
                    </Col>
                )}
            </>
        );
    } else {
        return null;
    }
};
