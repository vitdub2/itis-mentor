import {FC, useEffect, useState} from "react";
import { Typography, Col, Space } from 'antd';
import {API} from "../../api";
import {GroupItem} from "../../components/groups/GroupItem";
import {NavLink} from "react-router-dom";
import {getUserId} from "../../helpers/authHelper";

const { Title } = Typography;

export type IGroup = {
    id: string;
    name: string;
    subject: string;
    time: string;
    link: string;
    tutorId: string;
    color: string;
    peopleCount: string;

    place?: string;
}

export const MyGroupsPage: FC = () => {
    const [groups, setGroups] = useState<IGroup[] | undefined>();

    const fetchGroups = () => {
        API.group.getParticipatingLessons(getUserId() || "").then((data) => {
            setGroups(data);
        });
    }

    useEffect(() => {
        fetchGroups();
    }, []);

    return (
        <div>
            <Title>
                My groups
            </Title>
            <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                {groups && groups.map(group => (
                    <NavLink to={`/groups/${group.id}`}>
                        <GroupItem key={group.id} {...group} />
                    </NavLink>
                ))}
            </Space>
        </div>
    );
}
