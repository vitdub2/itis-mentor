import {FC, useEffect, useState} from "react";
import { Typography, Col, Space } from 'antd';
import {API} from "../../api";
import {GroupItem} from "../../components/groups/GroupItem";
import {NavLink} from "react-router-dom";
import {IGroup} from "./MyGroupsPage";

const { Title } = Typography;

export const AllGroupsPage: FC = () => {
    const [groups, setGroups] = useState<IGroup[] | undefined>();

    const fetchGroups = () => {
        API.group.getLessons().then((resp) => {
            setGroups(resp.data);
        });
    }

    useEffect(() => {
        fetchGroups();
    }, []);

    return (
        <div>
            <Title>
                All groups
            </Title>
            <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                {groups && groups.map(group => (
                    <NavLink to={`/groups/${group.id}`}>
                        <GroupItem key={group.id} {...group} />
                    </NavLink>
                ))}
            </Space>
        </div>
    );
}
