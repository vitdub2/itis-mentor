import {NavLink, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {Avatar, Card, Col, Descriptions, List, Row, Typography} from 'antd';
import {API} from "../../api";
import { IGroup } from "./MyGroupsPage";
import {getUserId} from "../../helpers/authHelper";

const { Title, Link } = Typography;

export type UserInfo = {
    id: number;
    login: string;
    photoUrl: number;
}

export type IGroupDetails = {
    id: number;
    groupId: number;
    peopleInfo: UserInfo[];
    tutor: UserInfo;
}

export const GroupPage = () => {
    const {id} = useParams();

    const [peopleInfo, setPeopleInfo] = useState<IGroupDetails>();
    const [groupInfo, setGroupInfo] = useState<IGroup>();

    useEffect(() => {
        if (id) {
            API.group.getLessonInfo(id).then((resp) => {
                setPeopleInfo(resp.data[0]);
            });
            API.group.getLesson(id).then((resp) => {
                setGroupInfo(resp.data[0]);
            });
        }
    }, [id])

    return (
        <div>
            {groupInfo ? (
                <>
                    <Title>
                        {groupInfo.name}
                    </Title>
                    <Descriptions title="Group info">
                        <Descriptions.Item label="Group name">
                            {groupInfo.name}
                        </Descriptions.Item>
                        <Descriptions.Item label="Subject">
                            {groupInfo.subject}
                        </Descriptions.Item>
                        <Descriptions.Item label="Time">
                            {groupInfo.time}
                        </Descriptions.Item>
                        {groupInfo.place && (
                            <Descriptions.Item label="Place">
                                {groupInfo.place}
                            </Descriptions.Item>
                        )}
                        <Descriptions.Item label="Chat">
                            <Link href={groupInfo.link}>
                                {groupInfo.link}
                            </Link>
                        </Descriptions.Item>
                    </Descriptions>
                </>
            ) : (
                <>Loading</>
            )}
            {peopleInfo && (
                <>
                    {peopleInfo.tutor && (
                        <>
                            <Title level={2}>
                                Tutor
                            </Title>
                            <NavLink to={`/user/${peopleInfo.tutor.id}`}>
                                <Card hoverable>
                                    <Row>
                                        <Col>
                                            <Avatar src={peopleInfo.tutor.photoUrl} size="large"/>
                                        </Col>
                                        <Col style={{
                                            marginLeft: "12px"
                                        }}>
                                            {peopleInfo.tutor.login}
                                        </Col>
                                    </Row>
                                </Card>
                            </NavLink>
                        </>
                    )}
                    {peopleInfo.peopleInfo && (
                        <>
                            <List
                                header={<Title level={2}>Participants</Title>}
                                // @ts-ignore
                                dataSource={peopleInfo.peopleInfo.filter((obj) => obj.id !== (+getUserId() || 0))}
                                renderItem={(info: UserInfo) => (
                                        <NavLink to={`/user/${info.id}`}>
                                            <Card hoverable>
                                                <Row>
                                                    <Col>
                                                        <Avatar src={info.photoUrl} size="large"/>
                                                    </Col>
                                                    <Col style={{
                                                        marginLeft: "12px"
                                                    }}>
                                                        {info.login}
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </NavLink>

                                )}
                            />
                        </>
                    )}
                </>
            )}
        </div>
    );
}
