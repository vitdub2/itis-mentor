import {Button, Form, Input, Col} from "antd";
import {FC, useContext, useState} from "react";
import {LoginData} from "../../../types/auth";
import {AuthContext} from "../../../contexts/AuthContext";
import {getDataFromError} from "../../../helpers/errorHelper";

export const LoginForm: FC = () => {
    const errorInitState = {_error: undefined};

    const {auth} = useContext(AuthContext);
    const [errors, setErrors] = useState(errorInitState);

    const clearErrors = () => {
        setErrors(errorInitState);
    }

    const submit = (values: LoginData) => {
        clearErrors();
        auth(values).catch((e) => {
            const info = getDataFromError(e);
            setErrors(info);
        });
    }

    const handleChange = () => {
        clearErrors();
    }

    return (
        <Form
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 8 }}
            onChange={handleChange}
            onFinish={submit}
        >
            <Form.Item
                label="Login"
                name="login"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input.Password />
            </Form.Item>

            {errors && errors._error && (
                <Col offset={8} className="ant-form-item-explain-error">
                    {errors._error}
                </Col>
            )}

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
