import {FC} from "react";
import {LoginForm} from "./LoginForm";
import {AuthPage} from "../AuthPage";

export const LoginPage: FC = () => {
    return (
        <AuthPage>
            <LoginForm/>
        </AuthPage>
    );
}
