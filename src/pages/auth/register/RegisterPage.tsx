import {FC} from "react";
import {AuthPage} from "../AuthPage";
import {RegisterForm} from "./RegisterForm";

export const RegisterPage: FC = () => {
    return (
        <AuthPage>
            <RegisterForm/>
        </AuthPage>
    );
}
