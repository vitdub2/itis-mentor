import {Button, Form, Input } from "antd";
import {FC, useContext} from "react";
import {AuthContext} from "../../../contexts/AuthContext";
import {RegisterData} from "../../../types/auth";

export const RegisterForm: FC = () => {
    const {register} = useContext(AuthContext);
    const submit = (values: RegisterData) => {
        register(values);
    }
    return (
        <Form
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 8 }}
            onFinish={submit}
        >
            <Form.Item
                label="Login"
                name="login"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                label="E-mail"
                name="email"
                rules={[{ required: true, message: 'Please input your email!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Photo url"
                name="photoUrl"
            >
                <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
