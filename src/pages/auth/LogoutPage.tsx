import {useContext, useEffect} from "react";
import { Navigate } from "react-router-dom";
import {AuthContext} from "../../contexts/AuthContext";

export const LogoutPage = () => {
    const {isAuth, logout} = useContext(AuthContext);

    useEffect(() => {
        logout();
    }, [logout]);

    if (!isAuth) {
        return (<Navigate to="/"/>);
    }

    return (<></>);
}
