import {useContext, useEffect} from "react";
import {Layout, Row, Col} from "antd";
import {Header} from "../../layout/Header";
import {Page} from "../../layout/Page";
import {AuthContext} from "../../contexts/AuthContext";
import {useNavigate} from "react-router-dom";
import {Component} from "../../types/common";

const {Content} = Layout;

export const AuthPage: Component = ({ children }) => {
    const {isAuth} = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (isAuth) {
            navigate("/");
        }
    }, [isAuth, navigate]);

    return (
        <Page>
            <Header/>
            <Content>
                <Row>
                    <Col span={22} offset={1}>
                        {children}
                    </Col>
                </Row>
            </Content>
        </Page>
    );
}
