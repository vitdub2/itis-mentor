import {FC, useContext} from "react";
import {AuthContext} from "../contexts/AuthContext";

import {Avatar, Row, Col, Typography, Input} from "antd";

const { Title, Paragraph } = Typography;

export const ProfilePage: FC = () => {
    const {userInfo} = useContext(AuthContext);
    return (
        <div>
            <Title>Profile</Title>
            <Row>
                <Col>
                    <Avatar src={userInfo.photoUrl} size="large"/>
                </Col>
                <Col>
                    <Title level={2}>
                        {userInfo.login}
                    </Title>
                </Col>
            </Row>
            <Row>
                <Paragraph>
                    Email: {userInfo.email}
                </Paragraph>
            </Row>
        </div>
    );
};
