import { Row, Col, Button, Space } from "antd";
import {FC} from "react";
import {Page} from "../layout/Page";
import {NavLink} from "react-router-dom";
import { CloseOutlined } from "@ant-design/icons";
import { Typography } from 'antd';

const { Title } = Typography;

export const NotFoundPage: FC = () => {
    return (
        <Page styles={{
            height: "100vh",
        }}>
            <Col style={{
                height: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
            }}>
                <Space direction="vertical" size="middle" style={{ alignItems: "center" }}>
                    <Row>
                        <CloseOutlined style={{
                            color: "red",
                            fontSize: "32px",
                        }}/>
                    </Row>
                    <Col>
                        <Title level={1}>
                            Not found
                        </Title>
                    </Col>
                    <Row>
                        <Button type="primary">
                            <NavLink to="/">
                                Back to safety
                            </NavLink>
                        </Button>
                    </Row>
                </Space>
            </Col>
        </Page>
    );
}
