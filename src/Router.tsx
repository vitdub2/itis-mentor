import {FC, useContext} from "react";
import {Route, Routes, Navigate} from "react-router-dom";
import {Main} from "./layout/Main";
import {LogoutPage} from "./pages/auth/LogoutPage";
import {LoginPage} from "./pages/auth/login/LoginPage";
import {AuthContext} from "./contexts/AuthContext";
import { NotFoundPage } from "./pages/NotFoundPage";
import {MyGroupsPage} from "./pages/groups/MyGroupsPage";
import {RegisterPage} from "./pages/auth/register/RegisterPage";
import {ProfilePage} from "./pages/ProfilePage";
import {GroupPage} from "./pages/groups/GroupPage";
import {UserPage} from "./pages/UserPage";
import {AllGroupsPage} from "./pages/groups/AllGroupsPage";

const ProtectedRoutes = () => {
    const {isAuth} = useContext(AuthContext);
    if (isAuth) {
        return <Main/>
    } else {
        return <Navigate to="/login"/>
    }
}

export const Router: FC = () => {
    return (
        <Routes>
            <Route path="/login" element={<LoginPage/>} />
            <Route path="/register" element={<RegisterPage/>} />
            <Route path="/" element={<ProtectedRoutes/>}>
                <Route path="groups">
                    <Route path=":id" element={<GroupPage/>}/>
                    <Route path="all" element={<AllGroupsPage/>}/>
                    <Route index element={<MyGroupsPage/>}/>
                </Route>
                <Route path="/user">
                    <Route path=":id" element={<UserPage/>}/>
                </Route>
                <Route index element={<ProfilePage/>}/>
            </Route>
            <Route path="/logout" element={<LogoutPage/>} />
            <Route path="*" element={<NotFoundPage/>}/>
        </Routes>
    );
}
