import {IGroup} from "../../pages/groups/MyGroupsPage";
import {Avatar, Badge, Card, Col, Row, Typography} from "antd";
import {UserOutlined, WechatOutlined } from "@ant-design/icons";

const { Link } = Typography;

export const GroupItem = ({
    color,
    name,
    time,
    place,
    subject,
    link,
    peopleCount,
}: IGroup) => {
    return (
        <Card
            hoverable
        >
            <Row>
                <Col>
                    <Avatar style={{ backgroundColor: color, verticalAlign: 'middle' }} size="large">
                        {name}
                    </Avatar>
                </Col>
                <Col style={{marginLeft: "12px"}}>
                    <Row>
                        <Badge text={subject} status="success"/>
                    </Row>
                    <Row>
                        <Badge text={time} status="warning"/>
                    </Row>
                    {place && (
                        <Row>
                            <Badge text={place} status="error"/>
                        </Row>
                    )}
                </Col>
                <Col style={{
                    marginLeft: "12px"
                }}>
                    <Row>
                        <Link href={link} style={{
                            display: "flex"
                        }}>
                            <WechatOutlined style={{
                                fontSize: "22px",
                                color: "#1890ff",
                                marginRight: "5px"
                            }}/>
                            <div>
                                Chat
                            </div>
                        </Link>
                    </Row>
                    <Row>
                        <UserOutlined style={{
                            fontSize: "22px",
                            color: "#1890ff",
                            marginRight: "5px"
                        }}/>
                        <Badge count={peopleCount} style={{
                            backgroundColor: '#1890ff'
                        }}/>
                    </Row>
                </Col>
            </Row>
        </Card>
    );
}
