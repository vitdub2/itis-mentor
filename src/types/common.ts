import {FC, ReactNode} from "react";

type WithChildren = {
    children: ReactNode;
}

export type Component<T = any> = FC<T & WithChildren>;
