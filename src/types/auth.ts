export type LoginData = {
    login: string;
    password: string;
}

export type RegisterData = LoginData & {
    email: string;
    photoUrl?: string;
}
