export const createError = (data: any) => {
    return new Error(JSON.stringify(data));
}

export const getDataFromError = (e: any) => {
    return JSON.parse(e.message);
}
