const LOCAL_STORAGE_NAME = `user`;

export const getUserId = () => {
    return localStorage.getItem(LOCAL_STORAGE_NAME);
}

export const clearUserId = () => {
    localStorage.removeItem(LOCAL_STORAGE_NAME);
}

export const setUserId = (id: any) => {
    localStorage.setItem(LOCAL_STORAGE_NAME, id);
}
